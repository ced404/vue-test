

// TODO: https://vuejs.org/v2/guide/list.html#key : vm.$set(vm.userProfile, 'age', 27) ?



// ## ROUTES

// 0. If using a module system (e.g. via vue-cli), import Vue and VueRouter
// and then call `Vue.use(VueRouter)`.

// 1. Define route components.
// These can be imported from other files
const Foo = { template: '<div>foo</div>' }
const Bar = { template: '<div>bar</div>' }

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
	{ path: '/foo', component: Foo },
	{ path: '/bar', component: Bar }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
	routes // short for `routes: routes`
})



// ## STORE

const store = new Vuex.Store({
	state: {
		message: '',
		toggleList : [
			{
				"id": 1,
				"name": "France",
				"pressed": false,
			},
			{
				"id": 2,
				"name": "England",
				"pressed": false,
			},
			{
				"id": 3,
				"name": "Italy",
				"pressed": true,
			},
		],
	},
	mutations: {
		initialiseStore (state) {
			console.log('initialiseStore', localStorage.getItem ('toggleList'));
			if (! localStorage.getItem ('toggleList')) return;
			state.toggleList = JSON.parse (localStorage.getItem ('toggleList'));
		},
		updateToggleList (state, payload) {
			state.toggleList.find (x => x.id === payload.id).pressed = payload.pressed;
			let data = JSON.stringify (state.toggleList);
			localStorage.setItem ('toggleList', data);
		},
		updateMessage (state, payload) {
			console.log (payload.bodyText);
			state.message = payload.bodyText;
		}
	},
	getters: {
		loadToggleList (state) {
			return state.toggleList;
		},

	},
	actions: {
		refreshMessage (context) {
			return new Promise ((resolve) => {
				Vue.http.get ('https://jsonplaceholder.typicode.com/posts').then ((response) => {
					context.commit ('updateMessage', response);
					resolve (JSON.parse (response.bodyText));
				});
			});
		}
	},

});





// ## COMPONENTS

Vue.component ('toggle-button', {

	template: '#toggleButton',

	// default component state from attribute
	props: [
		'pressed',
		'id'
	],

	// component's data
	data: function () {
		return {
			// set initial state from attribute (props)
			'isPressed': this.pressed,
		}
	},

	// component's methods
	methods: {
		// toggle-button component state on/off
		// and emit event to parent when pressed
		handlePressed: function (value) {
			this.isPressed = !this.isPressed;
			let toggleState = {
				"id": this.id,
				"pressed": this.isPressed, // or "value"
			};
			console.log ('handlePressed', toggleState);
			this.$parent.$emit ('toggle-button-pressed', toggleState);
		}
	},

	/*
	watch: {
		// emit an event when "isPressed" changes
		isPressed: function (value, oldValue) {
			this.handlePressed (value);
		},
	}*/
});




// ## MAIN APP
var app = new Vue ({

	http: {
		root: '/',
		headers: {}
	},
	router,
	el: '#app',

	data: {
		toggleList: {},
		message: 'empty',
	},

	created () {
		// get eventual previous state from localStorage
		store.commit ('initialiseStore');
		// load toggle-buttons
		this.toggleList = this.getToggles();
	},

	mounted () {
		// bind events
		this.$on ('toggle-button-pressed', function (results) {
			this.saveToggle (results);
		});
	},

	methods: {
		getToggles () {
			return store.getters.loadToggleList;
		},
		saveToggle (toggle) {
			console.log ('saveToggle', toggle);
			store.commit ('updateToggleList', toggle);
			this.refreshMessage ();
		},
		refreshMessage () {
			this.message = "loading…";
			store.dispatch('refreshMessage').then ((results) => {
				console.log('done refreshing message', results);
				this.message = results[0];
			});
		},
	},

	computed: {
		// limitedChecked: function () {
		// 	if (this.checked.length > 2) return this.checked.pop();
		// 	return this.checked;
		// }
	}


}).$mount('#app');




/*
const Users2 = {
el: '#app',

data() {
return {
userList: {},
selectedUser: null
}
},

mounted() {
this.userList = this.getUsers()
},

methods: {
setSelectedUser(user) {
this.selectedUser = user
},
getUsers() {
return {... userList}
}
},

components: {
'user-rating': Rating2
}
}
*/
